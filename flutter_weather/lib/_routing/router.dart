import 'package:flutter/material.dart';
import 'package:flutter_weather/_routing/routes.dart';
import 'package:flutter_weather/views/home.dart';
import 'package:flutter_weather/views/spring.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case homeViewRoute:
      return MaterialPageRoute(builder: (context) => HomePage());
    case springViewRoute:
      return MaterialPageRoute(builder: (context) => SpringPage());
      break;
    default:
      return MaterialPageRoute(builder: (context) => HomePage());
  }
}
