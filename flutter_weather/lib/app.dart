import 'package:flutter/material.dart';
import 'package:flutter_weather/_routing/router.dart' as router;
import 'package:flutter_weather/theme.dart';
import '_routing/routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Social',
      debugShowCheckedModeBanner: false,
      theme: buildThemeData(),
      onGenerateRoute: router.generateRoute,
      initialRoute: homeViewRoute,
    );
  }
}
