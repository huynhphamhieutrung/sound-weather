import 'package:flutter/material.dart';
import 'package:flutter_weather/views/spring.dart';
import 'package:weather_icons/weather_icons.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    DateTime now = new DateTime.now();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            toolbarHeight: 60,
            bottom: TabBar(
              indicatorColor: Colors.lightBlue,
              indicatorSize: TabBarIndicatorSize.label,
              tabs: [
                IconButton(
                  icon: Icon(WeatherIcons.day_haze),
                ),
                IconButton(
                  icon: Icon(WeatherIcons.day_sleet),
                ),
                IconButton(
                  icon: Icon(WeatherIcons.day_light_wind),
                ),
                IconButton(
                  icon: Icon(WeatherIcons.night_sleet),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              SpringPage(),
              SpringPage(),
              SpringPage(),
              SpringPage(),
            ],
          ),
        ),
      ),
    );
  }
}
